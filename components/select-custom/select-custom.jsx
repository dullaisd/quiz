import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

import { useScrollLock } from '@hooks/use-scroll-lock';
import { useDeviceType } from '@hooks/use-device-type';

const SelectCustom = ({ error, options, value, placeholder, onChange, className }) => {
	const [usingPlaceholder, setUsingPlaceholder] = useState(true);
	const [chosenTitle, setChosenTitle] = useState(placeholder || '');
	const lockScroll = useScrollLock(false);
	const device = useDeviceType();

	const select = useRef(null);

	const getChosen = () => {
		return options.find((option) => option.value === value) || null;
	};

	const checkChosen = () => {
		const chosen = getChosen();

		if (chosen !== null) {
			setUsingPlaceholder(false);
			setChosenTitle(chosen.title);
		} else {
			setUsingPlaceholder(true);
			setChosenTitle(placeholder || '');
		}
	};

	useEffect(() => {
		checkChosen();
	}, [value]);

	return (
		<div
			ref={select}
			className={classnames({
				'select-custom': true,
				[className]: typeof className === 'string',
				placeholder: usingPlaceholder,
				error
			})}
			tabIndex="-1"
			onFocus={() => {
				if (device !== 'desktop') {
					lockScroll(true);
				}
			}}
			onBlur={() => {
				if (device !== 'desktop') {
					lockScroll(false);
				}
			}}
		>
			<div className="content">
				<div className="value">{chosenTitle}</div>
				<div className="arrow" />
				<div className="dropdown">
					{options.map((option) => {
						return (
							<div
								onClick={() => {
									select.current.blur();
									onChange(option);
								}}
								key={option.id}
								className="option"
							>
								{option.title}
							</div>
						);
					})}
				</div>
				{device !== 'desktop' && (
					<div
						onClick={() => {
							select.current.blur();
						}}
						className="cover"
					/>
				)}
			</div>
		</div>
	);
};

SelectCustom.propTypes = {
	placeholder: PropTypes.string,
	options: PropTypes.array.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	onChange: PropTypes.func.isRequired,
	className: PropTypes.string,
	error: PropTypes.bool
};

export default SelectCustom;
