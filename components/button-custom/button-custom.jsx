import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

/** Simple `<button>` element that holds default styles. Takes up 100% of with by default  */
const ButtonCustom = ({ error, children, className, ...props }) => {
	return (
		<button
			className={classnames({ 'button-custom': true, [className]: typeof className === 'string' })}
			{...props}
		>
			{children}
		</button>
	);
};

ButtonCustom.propTypes = {
	children: PropTypes.string,
	error: PropTypes.bool,
	className: PropTypes.string
};

ButtonCustom.defaultProps = {
	children: 'No title'
};

export default ButtonCustom;
