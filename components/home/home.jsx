import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

import CenterContainer from '@components/center-container/center-container';
import InputCustom from '@components/input-custom/input-custom';
import SelectCustom from '@components/select-custom/select-custom';
import ButtonCustom from '@components/button-custom/button-custom';

import { useDeviceType } from '@hooks/use-device-type';

const Home = ({ quizes, setQuizState }) => {
	const [btnTitle, setBtnTitle] = useState('go ');
	const [name, setName] = useState('');
	const [chosenQuizId, setChosenQuizId] = useState('');
	const [errors, setErrors] = useState({
		name: false,
		quiz: false
	});
	const device = useDeviceType();

	const parsedOptions = quizes.map((entry) => {
		return {
			id: entry.id,
			title: entry.title,
			value: entry.id
		};
	});

	const handleSubmit = (e) => {
		e.preventDefault();

		setErrors((state) => {
			const newState = { ...state, name: name.length < 1, quiz: chosenQuizId === '' };

			// Passed validation
			if (!newState.name && !newState.quiz) {
				setQuizState((state) => {
					return { ...state, quizId: chosenQuizId, name };
				});
			}

			return newState;
		});
	};

	useEffect(() => {
		let title = 'go ';

		const timer = setInterval(() => {
			// 3 characters per 'go '
			let amount = Math.round(title.length / 3, 10) + 1;

			if (amount > 10) {
				amount = 1;
			}

			title = [...Array(amount)].map(() => 'go ').join('');

			setBtnTitle(title);
		}, 100);

		return () => {
			clearInterval(timer);
		};
	}, []);

	return (
		<CenterContainer titleSize={device === 'phone' ? 40 : 82} title="Quiz time !!!">
			<form className="initial-form" onSubmit={handleSubmit}>
				<InputCustom
					type="text"
					name="Name"
					value={name}
					onChange={(e) => {
						setErrors((state) => {
							return { ...state, name: false };
						});
						setName(e.target.value);
					}}
					placeholder="Enter your name"
					error={errors.name}
				/>

				<SelectCustom
					value={chosenQuizId}
					options={parsedOptions}
					onChange={(value) => {
						setErrors((state) => {
							return { ...state, quiz: false };
						});
						setChosenQuizId(value.value);
					}}
					placeholder="Choose test"
					error={errors.quiz}
				></SelectCustom>

				<ButtonCustom>{btnTitle}</ButtonCustom>
			</form>
		</CenterContainer>
	);
};

Home.propTypes = {
	quizes: PropTypes.array.isRequired,
	setQuizState: PropTypes.func.isRequired
};

export default Home;
