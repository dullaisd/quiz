import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const StatusBar = ({ percents }) => {
	return <div style={{ '--percents': `${percents}%` }} className="status-bar"></div>;
};

StatusBar.propTypes = {
	percents: PropTypes.number.isRequired
};

export default StatusBar;
