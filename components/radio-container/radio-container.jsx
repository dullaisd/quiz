import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

import { randString } from '@utils/utils';

const RadioContainer = ({ children, className, ...props }) => {
	const [id] = useState(randString());

	return (
		<div
			suppressHydrationWarning={true}
			className={classnames({ 'radio-container': true, [className]: typeof className === 'string' })}
		>
			<input {...props} type="radio" id={id} />
			<label className="container" htmlFor={id}>
				{children}
			</label>
		</div>
	);
};

RadioContainer.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string
};

export default RadioContainer;
