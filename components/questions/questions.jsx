import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

import CenterContainer from '@components/center-container/center-container';
import RadioContainer from '@components/radio-container/radio-container';
import Toast from '@components/toast/toast';
import ButtonCustom from '@components/button-custom/button-custom';
import StatusBar from '@components/status-bar/status-bar';

import { getQuestions, getAnswers } from '@utils/api-calls';
import { genSnarkyComment } from '@utils/utils';
import { useDeviceType } from '@hooks/use-device-type';

const Questions = ({ state, setQuizState }) => {
	const [questions, setQuestions] = useState([]),
		[toastVisible, setToastVisible] = useState(true),
		[toestText, setToestText] = useState('So you know your name... Good luck !'),
		[answers, setAnswers] = useState([]),
		[checked, setChecked] = useState(null),
		grumpyTimer = useRef(null),
		currentQuestion = questions[state.questionIndex] || { title: '' },
		device = useDeviceType();

	const handleClose = () => {
		setToastVisible(false);

		grumpyTimer.current = setTimeout(() => {
			setToestText("You won't get rid of me that easily..");
			setToastVisible(true);

			grumpyTimer.current = setTimeout(() => {
				setToestText(genSnarkyComment());
			}, 2000);
		}, 1000);
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		if (checked !== null) {
			setToestText(genSnarkyComment());

			setQuizState((state) => {
				return {
					...state,
					done: state.questionIndex >= questions.length - 1,
					answers: { ...state.answers, [state.questionIndex]: checked },
					questionIndex: state.questionIndex + 1
				};
			});

			setChecked(null);
		}
	};

	useEffect(() => {
		(async () => {
			try {
				const response = await getQuestions(state.quizId),
					questions = response.data;

				setQuestions(questions);
			} catch (err) {
				setToestText("Error 500, let's not dwell on it");
			}
		})();

		return () => {
			clearTimeout(grumpyTimer.current);
		};
	}, []);

	useEffect(() => {
		if (typeof currentQuestion.id !== 'undefined' && typeof answers[currentQuestion.id] === 'undefined') {
			(async () => {
				try {
					setAnswers([]);
					const response = await getAnswers(state.quizId, currentQuestion.id),
						answers = response.data;

					setAnswers(answers);
				} catch (err) {
					setToestText("Error 500, let's not dwell on it");
				}
			})();
		}
	}, [state.quizId, currentQuestion.id]);

	return (
		<div className="questions">
			<CenterContainer titleSize={26} title={currentQuestion.title}>
				<form className="container" onSubmit={handleSubmit}>
					{answers.map((answer) => {
						return (
							<RadioContainer
								className="option"
								key={answer.id}
								name="Answer button"
								onChange={() => {
									setToestText(genSnarkyComment());
									setChecked(answer.id);
								}}
							>
								{answer.title}
							</RadioContainer>
						);
					})}

					<StatusBar percents={Math.floor((100 / questions.length) * state.questionIndex)}></StatusBar>

					<ButtonCustom className="button">Next</ButtonCustom>
				</form>
			</CenterContainer>

			{device !== 'phone' && toastVisible && <Toast onClose={handleClose}>{toestText}</Toast>}
		</div>
	);
};

Questions.propTypes = {
	state: PropTypes.object.isRequired,
	setQuizState: PropTypes.func.isRequired
};

export default Questions;
