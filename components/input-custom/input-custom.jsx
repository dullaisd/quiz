import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const InputCustom = ({ error, className, ...props }) => {
	return (
		<input
			className={classnames({ 'custom-input': true, error, [className]: typeof className === 'string' })}
			{...props}
			type="text"
		/>
	);
};

InputCustom.propTypes = {
	className: PropTypes.string,
	error: PropTypes.bool
};

export default InputCustom;
