import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

import CenterContainer from '@components/center-container/center-container';
import Toast from '@components/toast/toast';
import ButtonCustom from '@components/button-custom/button-custom';

import { getResults } from '@utils/api-calls';
import { genSnarkyComment } from '@utils/utils';

const Results = ({ quizState, setQuizState }) => {
	const [state, setState] = useState({ correct: 0, total: 0 }),
		[toastVisible, setToastVisible] = useState(true),
		[toestText, setToestText] = useState(genSnarkyComment());

	const resetQuiz = () => {
		setQuizState({ quizId: null, name: '', questionIndex: 0, answers: {}, done: false });
	};

	useEffect(() => {
		(async () => {
			try {
				const response = await getResults(quizState.quizId, quizState.answers),
					{ data } = response;

				setState(data);
			} catch (err) {
				setToestText("Error 500, let's not dwell on it");
			}
		})();
	}, []);

	return (
		<div className="results">
			<CenterContainer titleSize={26} title={`Thanks, ${quizState.name}`}>
				<div className="text">{`You responded correctly to ${state.correct} out of ${state.total} questons.`}</div>
				<ButtonCustom onClick={resetQuiz} className="button">
					I can do better
				</ButtonCustom>
			</CenterContainer>

			{toastVisible && (
				<Toast
					onClose={() => {
						setToastVisible(false);
					}}
				>
					{toestText}
				</Toast>
			)}
		</div>
	);
};

Results.propTypes = {
	quizState: PropTypes.object.isRequired,
	setQuizState: PropTypes.func.isRequired
};

export default Results;
