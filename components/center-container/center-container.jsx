import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const CenterContainer = ({ titleSize, children, title }) => {
	return (
		<main style={{ '--title-size': `${titleSize}px` }} className="center-container">
			<h1>{title}</h1>
			{children}
		</main>
	);
};

CenterContainer.propTypes = {
	children: PropTypes.node.isRequired,
	title: PropTypes.string.isRequired,
	titleSize: PropTypes.number
};

CenterContainer.defaultProps = {
	titleSize: 82
};

export default CenterContainer;
