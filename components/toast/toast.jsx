import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const Toast = ({ children, onClose }) => {
	return (
		<div className={classnames({ toast: true, grumpy: true })}>
			<div className="text">{children}</div>
			<div className="grumpy"></div>
			<div className="close" onClick={onClose}></div>
		</div>
	);
};

Toast.propTypes = {
	children: PropTypes.node,
	grumpy: PropTypes.bool,
	onClose: PropTypes.func
};

Toast.defaultProps = {
	// Better that do logic
	onClose: () => {}
};

export default Toast;
