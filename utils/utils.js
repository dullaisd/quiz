const quizKey = 'quizState';

const snarkyComments = [
	'I like you. You remind me of me when I was young and stupid.',
	'I don’t know what your problem is, but I’ll bet it’s hard to pronounce.',
	'How about never? Is never good for you?',
	'I see you’ve set aside this special time to humiliate yourself in public.',
	'I’m really easy to get along with once you people learn to worship me.',
	'Well, aren’t we a bloody ray of sunshine?',
	'I’m out of my mind, but feel free to leave a message.',
	'I said, “No.” Which word don’t you understand?',
	'I refuse to have a battle of wits with an unarmed person.',
	'It sounds like English, but I can’t understand a word you’re saying.',
	'If things get any worse, I’ll have to ask you to stop helping me.',
	'I can see your point, but I still think you’re full of crap.',
	'You are validating my inherent mistrust of co-workers.',
	'I have plenty of talent and vision. I just don’t give a damn.',
	'I’m already visualizing the duct tape over your mouth.',
	'I will always cherish the initial misconceptions I had about you.',
	'The fact that no one understands you doesn’t make you an artist.',
	'Any connection between your reality and mine is purely coincidental.',
	'What am I? Flypaper for freaks!?',
	'I’m not being rude. You’re just insignificant.',
	'It’s a thankless job, but I’ve got a lot of Karma to burn off.',
	'Yes, I am an agent of Satan, but my duties are largely ceremonial.',
	'No, my powers can only be used for good.',
	'You sound reasonable. Did you take your meds today?',
	'Who me? I just wander from room to room.',
	'Don’t worry. I forgot your name, too.',
	'You say I’m a bitch like it’s a bad thing.',
	'Thank you. We’re all refreshed and challenged by your unique point of view.',
	'Don’t bother me, I’m living happily ever after.',
	'This isn’t an office. It’s HELL with fluorescent lighting.',
	'Therapy is expensive. Popping bubble wrap is cheap. You choose.',
	'I’m not crazy. I’ve been in a very bad mood for the last 30 years.',
	'Sarcasm is just one more service I offer.',
	'Do they ever shut up on your planet?',
	'Back off!! You’re standing in my aura.',
	'Well, this day was a total waste of make-up.',
	'I work 50 hours a week to be this poor.',
	'I’ll try being nicer if you’ll try being smarter.',
	'Ambivalent? Well, yes and no.',
	'Earth is full. Go home.',
	'Aw, did I step on your poor itty bitty ego?',
	'You are depriving some village of their idiot.',
	'Whatever kind of look you were going for, you missed.',
	'Wait…I’m trying to imagine you with a personality.',
	'We’ve been friends for a very long time. How about we call it quits?',
	'Ahhh. I see the SNAFU fairy has visited us again.',
	'Oh I get it … like humor … but different.'
];

// Generates random string
export const randString = () => {
	return Math.random()
		.toString(36)
		.substr(2, 9);
};

// Saves quiz id into localStorage
export const saveQuizState = (state) => {
	localStorage.setItem(quizKey, JSON.stringify(state));

	return true;
};

// Gets quiz id from localStorage
export const getQuizState = () => {
	const state = localStorage.getItem(quizKey);

	// Iam no fan of falsy values
	if (state === null) {
		return false;
	}

	return JSON.parse(state);
};

// Returns true if code runs on server
export const isServer = () => {
	return typeof window === 'undefined';
};

// Returns string representing device type based on matchMedia
export const getDeviceType = () => {
	const desktop = '(min-width: 1025px)',
		phone = '(max-width: 767px)';

	if (isServer()) {
		return null;
	}

	if (window.matchMedia(phone).matches) {
		return 'phone';
	}

	if (window.matchMedia(desktop).matches) {
		return 'desktop';
	}

	return 'tablet';
};

// Gets dom node offset
export const domNodeOffset = (el) => {
	if (!isServer()) {
		const rect = el.getBoundingClientRect(),
			scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
			scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
	}
};

// Returns snarky comment
export const genSnarkyComment = () => {
	return snarkyComments[Math.floor(Math.random() * snarkyComments.length)];
};
