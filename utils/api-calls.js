import axios from 'axios';

axios.defaults.baseURL = 'https://printful.com/';

const call = async ({ endpoint, method = 'GET', data }) => {
	const axiosConfig = {
		url: endpoint,
		method,
		...(method === 'GET' && typeof data !== 'undefined' ? { params: data } : {}),
		...(method !== 'GET' && typeof data !== 'undefined' ? { data } : {})
	};

	return axios.request(axiosConfig);
};

// Returns quizes
export const getQuizess = () => {
	return call({
		endpoint: 'test-quiz.php',
		data: { action: 'quizzes' }
	});
};

// Returns question by id
export const getQuestions = (quizId) => {
	return call({
		endpoint: 'test-quiz.php',
		data: { action: 'questions', quizId }
	});
};

// Returns answers by quiz and question id
export const getAnswers = (quizId, questionId) => {
	return call({
		endpoint: 'test-quiz.php',
		data: { action: 'answers', quizId, questionId }
	});
};

// Gets results
export const getResults = (quizId, answers) => {
	return call({
		endpoint: 'test-quiz.php',
		data: { action: 'submit', quizId, answers: Object.entries(answers).map((entry) => entry[1]) }
	});
};
