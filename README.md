# Quiz

## About

Simple questionnaire app. Built on top of [nextjs](https://nextjs.org/). Uses localStorage to back up progress on every action. Supports SSR and Static generation (move getInitialProps to effect and it will build whole thing as sstatic site. At this point we populate quizes server side for faster load times). **No ui or ux plugins are used here**

## Main stack:

-   [nextjs](https://nextjs.org/)
-   [react](https://nextjs.org/)
-   [axios](https://reactjs.org/)

There is also sass, babel prettier and eslint set up. There is no need for context api or redux on such a small project - so i didnt use any of those.

## Folder structure:

`./components` - All components are here. There is no need for complicated folder structure on such a small projct

`./hooks` - custom hooks

`/.pages` - root components

`./static` - static files

`./utils` Utilitiy functions and api calls

## Npm scripts

`build` - Buils production version

`start` - Starts production version

`dev` - starts development server
