import { useState, useEffect, useRef } from 'react';
import { isServer, getDeviceType } from '@utils/utils';

/** Allows to inject device type react style
 * @returns {'desktop'|'tablet'|'phone'} String representing device type
 */
export const useDeviceType = () => {
	const [type, setType] = useState(getDeviceType()),
		timer = useRef(null);

	const listen = (e) => {
		clearTimeout(timer.current);

		// debounce
		timer.current = setTimeout(() => {
			setType(getDeviceType());
		}, 50);
	};

	useEffect(() => {
		if (!isServer()) {
			window.addEventListener('resize', listen);
		}

		return () => {
			if (!isServer()) {
				window.removeEventListener('resize', listen);
			}
		};
	}, []);

	return type;
};
