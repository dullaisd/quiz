import React, { useEffect, useState, useRef } from 'react';

import { isServer } from '@utils/utils';

/**
 * @typedef scrollLockFn
 * @type {function}
 * @param {boolean} shouldLock If true - locks scroll, if false - unlocks it
 */

/** Allows us to lock screen
 * @returns {scrollLockFn} Function to be used
 */
export const useScrollLock = () => {
	const [lastScrollTop, setLastScrollTop] = useState();
	const [locked, setLocked] = useState(false);
	const lastStyle = useRef(null);

	if (isServer()) {
		return null;
	}

	const fn = (shouldLock = !locked) => {
		if (shouldLock && !locked) {
			const scrollTop = window.scrollY;

			lastStyle.current = document.body.style.cssText;
			setLastScrollTop(scrollTop);

			document.body.style.cssText = `position: fixed; width: 100%; top:${-scrollTop}px; overflow: hidden;`;

			setLocked(true);
		} else if (!shouldLock && locked) {
			document.body.style.cssText = lastStyle.current;
			window.scrollTo(0, lastScrollTop);

			setLocked(false);
		}
	};

	// When removed, unlocks screen if it's left locked
	useEffect(() => {
		return () => {
			if (locked) {
				fn(false);
			}
		};
	}, []);

	return fn;
};
