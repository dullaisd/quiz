import React from 'react';

import { isServer, domNodeOffset } from '@utils/utils';

export const useScrollTo = () => {
	if (isServer()) {
		return null;
	}

	return (to, offset = 0) => {
		window.scrollTo(0, (typeof to === 'number' ? to : domNodeOffset(to).top) + offset);
	};
};
