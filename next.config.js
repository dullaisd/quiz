/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-param-reassign */
const withSass = require('@zeit/next-sass');
const withCss = require('@zeit/next-css');
const withOptimizedImages = require('next-optimized-images');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const path = require('path');

module.exports = withOptimizedImages(
	withSass(
		withCss({
			cssModules: false,
			optimizeImages: false,
			webpack(config) {
				config.resolve.alias['@components'] = path.resolve(__dirname, 'components/');
				config.resolve.alias['@hooks'] = path.resolve(__dirname, 'hooks/');
				config.resolve.alias['@utils'] = path.resolve(__dirname, 'utils/');
				config.resolve.alias['@scss'] = path.resolve(__dirname, 'scss/_index.scss');
				config.resolve.plugins = config.resolve.plugins || [];
				config.plugins = config.plugins || [];

				config.plugins.push(
					// Turning off "Conflicting order between" warnings for webpack because they get generates as
					// soon as 2 components import styles in different order. It would be nightmare to manage that, as well as our components are scoped individually
					new FilterWarningsPlugin({
						exclude: [/mini-css-extract-plugin[^]*Conflicting order between:/]
					})
				);

				// Building webpack analyzer report when building production
				if (process.env.NODE_ENV === 'production') {
					config.plugins.push(new BundleAnalyzerPlugin({ analyzerMode: 'static', openAnalyzer: false }));
				}

				return config;
			}
		})
	)
);
