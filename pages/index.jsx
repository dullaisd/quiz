import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Home from '@components/home/home';
import Questions from '@components/questions/questions';
import Results from '@components/results/results';
import { getQuizess } from '@utils/api-calls';
import { getQuizState, saveQuizState } from '@utils/utils';

const Index = ({ quizes }) => {
	const [quizState, setQuizState] = useState({
		quizId: null,
		name: '',
		questionIndex: 0,
		answers: {},
		done: false
	});

	// Gets state on initial load
	useEffect(() => {
		const quizState = getQuizState();

		if (quizState !== false) {
			setQuizState(quizState);
		}
	}, []);

	// This will run twice on initialization, but it's small price to pay for eas of use and elegancy
	useEffect(() => {
		saveQuizState(quizState);
	}, [quizState]);

	if (quizState.done === true) {
		return <Results quizState={quizState} setQuizState={setQuizState}></Results>;
	}

	if (quizState.quizId === null) {
		return <Home setQuizState={setQuizState} quizes={quizes}></Home>;
	}

	return <Questions state={quizState} setQuizState={setQuizState}></Questions>;
};

Index.propTypes = {
	quizes: PropTypes.array.isRequired
};

Index.getInitialProps = async () => {
	try {
		const response = await getQuizess();

		return {
			quizes: response.data
		};
	} catch (err) {
		return {
			quizes: []
		};
	}
};
export default Index;
