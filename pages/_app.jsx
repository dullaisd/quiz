import App from 'next/app';
import React from 'react';

import '../common.scss';

// Basically using this only for common styles at this point...
class MyApp extends App {
	render() {
		const { Component, pageProps } = this.props;

		return <Component {...pageProps} />;
	}
}

export default MyApp;
